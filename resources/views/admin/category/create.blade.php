@extends('template.test2')

@section('title')
    Tambah Kategori
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
        <form method="POST" action="/admin/category">
          @csrf
            <div class="form-group">
              <label for="title">Nama Kategori</label>
              <input type="text" class="form-control @error('title') is-invalid @enderror" id="" name="title" value="{{ old('title') }}" autofocus>
              @error('title')
                  <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
    </div>
  </div>
@endsection