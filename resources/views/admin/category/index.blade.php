@extends('template.test2')

@section('title')
    Kategori
@endsection

@push('style')
    <link rel="stylesheet" type="text/css" href="https://cdn.datatables.net/v/bs4/dt-1.11.3/datatables.min.css"/>
@endpush

@section('content')
    <div class="card">
        <div class="card-header">
            <a href="/admin/category/create" class="btn btn-success btn-sm"><i class="fas fa-plus"></i> Tambah Kategori</a>
        </div>
        <div class="card-body">
            <table id="table-1" class="table table-bordered table-striped">
                <thead>
                    <tr>
                        <th>#</th>
                        <th>Nama Category</th>
                        <th>Action</th>
                    </tr>
                </thead>
                <tbody>
                    @forelse ($category as $key => $item)
                        <tr>
                            <td>{{$key+1}}</td>
                            <td>{{$item->title}}</td>
                            <td>
                                <form action="/admin/category/{{$item->id}}" method="POST">
                                    @csrf
                                    @method('DELETE')
                                    <a href="/admin/category/{{$item->id}}/edit" class="btn btn-warning btn-sm">Update</a>
                                    <input type="submit" value="Delete" class="btn btn-sm btn-danger">
                                </form>
                            </td>
                        </tr>
                    @empty
                        <tr>
                            <td colspan="3" class="text-center">Tidak ada Data!</td>
                        </tr>
                    @endforelse
                    
              </table>
        </div>
    </div>
@endsection

@push('script')
    <script src="/plugins/datatables/jquery.dataTables.js"></script>
    <script src="/plugins/datatables-bs4/js/dataTables.bootstrap4.js"></script>
    <script>
        $(function () {
            $("#table-1").DataTable();
    });
    </script>
@endpush