@extends('template.test2')

@section('title')
    Edit Kategori
@endsection

@section('content')
  <div class="card">
    <div class="card-body">
        <form method="POST" action="/admin/category/{{$category->id}}">
          @csrf
          @method('PUT')
            <div class="form-group">
              <label for="title">Nama Kategori</label>
              <input type="text" class="form-control @error('title') is-invalid @enderror" id="" name="title" value="{{$category->title}}" autofocus>
              @error('title')
                  <span class="text-danger">{{$message}}</span>
              @enderror
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
    </div>
  </div>
@endsection